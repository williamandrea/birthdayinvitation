import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const firebaseConfig = {
  apiKey: "AIzaSyCyFbgoAsA467t_0kvTb7Ce0peTWOo7u2Q",
  authDomain: "birthday-invitation-adba3.firebaseapp.com",
  databaseURL: "https://birthday-invitation-adba3.firebaseio.com",
  projectId: "birthday-invitation-adba3",
  storageBucket: "birthday-invitation-adba3.appspot.com",
  messagingSenderId: "660265719896",
  appId: "1:660265719896:web:c788f5d814b35920cb122d"
};

export default firebase.initializeApp(firebaseConfig);