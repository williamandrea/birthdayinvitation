const initialState = {
  isLoading: false,
  user: {},
  numberJoin: null,
  isJoin: null,
  chatList: [],
}

const reducer = (state = initialState, action) => {
  if (action.type === 'SET_LOADING') {
    return {
      ...state,
      isLoading: action.value
    }
  }
  if (action.type === 'SET_USER') {
    return {
      ...state,
      user: action.value
    }
  }
  if (action.type === 'SET_CHATLIST') {
    return {
      ...state,
      chatList: action.value
    }
  }
  if (action.type === 'SET_NUMBERJOIN') {
    return {
      ...state,
      numberJoin: action.value
    }
  }
  if (action.type === 'SET_JOIN') {
    return {
      ...state,
      isJoin: action.value
    }
  }
  return state
}

export default reducer
