import firebase from '../firebase'

export const registerUser = (data) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({type: 'SET_LOADING', value: true})
    firebase.auth().createUserWithEmailAndPassword(data.email, data.password)
      .then(res => {
        res.user.updateProfile({
          displayName: data.fullName,
        })
        dispatch({type: 'SET_LOADING', value: false})
        resolve(true)
      })
      .catch(err => {
        dispatch({type: 'SET_LOADING', value: false})
        reject(false)
      })
  })
}

export const loginUser = (data) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({type: 'SET_LOADING', value: true})
    firebase.auth().signInWithEmailAndPassword(data.email, data.password)
      .then(res => {
        const user = {
          displayName: res.user.displayName,
          email: res.user.email,
          uid: res.user.uid,
        }
        dispatch({type: 'SET_USER', value: user})
        dispatch({type: 'SET_LOADING', value: false})
        resolve(true)
      })
      .catch(err => {
        dispatch({type: 'SET_LOADING', value: false})
        reject(false)
      })
  })
}

export const setData = (data) => (dispatch) => {
  return new Promise((resolve, reject) => {
    try {
      AsyncStorage.setItem(data.key, data.value)
      resolve(true)
    }catch (error) {
      console.log('setData', error)
      reject(false)
    }
  })
}

export const getData = (data) => (dispatch) => {
  return new Promise((resolve, reject) => {
    try {
      AsyncStorage.getItem(data.key).then(res => {
        if (res !== null){
          resolve(res)
        }
      })
    }catch (error){
      console.log('getData', error)
      reject(false)
    }
  })
}

export const getUser = () => (dispatch) => {
  return new Promise((resolve, reject) => {
    try{
      const currentUser = firebase.auth().currentUser
      dispatch({type: 'SET_USER', value: currentUser})
      resolve(currentUser)
    }catch (error){
      reject(false)
    }
  })
}

export const setUser = (data) => (dispatch) => {
  console.log('SET_USER', data.user)
  return new Promise((resolve, reject) => {
    try{
      dispatch({type: 'SET_USER', value: data.user})
      resolve(true)
    }catch (error){
      reject(false)
    }
  })
}

export const sendChat = (data) => (dispatch) => {
  const url = firebase.database().ref(`chatlist`)
  return new Promise((resolve, reject) => {
    url.push({
      uid: data.uid,
      displayName: data.displayName,
      timestamp: data.timestamp,
      text: data.text
    })
  })
}

export const getChat = () => (dispatch) => {
  const url = firebase.database().ref(`chatlist`)
  return new Promise((resolve, reject) => {
    url.on('value', (snapshot) => {
      const data = []
      if (snapshot.numChildren() > 0){
        Object.keys(snapshot.val()).map(key => {
          data.push({
            id: key,
            data: snapshot.val()[key]
          })
        })
      }
      dispatch({type: 'SET_CHATLIST', value: data})
      resolve(data)
    })
  })
}

export const setUserJoin = (data) => (dispatch) => {
  const url = firebase.database().ref(`joinParty/${data.uid}`)
  return new Promise((resolve, reject) => {
    url.set({
      isJoin: data.isJoin
    })
    dispatch({type: 'SET_JOIN', value: data.isJoin})
    console.log('SETUSERJOIN', data.isJoin)
  })
}

export const getUserJoin = (data) => (dispatch) => {
  const url = firebase.database().ref(`joinParty/${data.uid}`)
  return new Promise((resolve, reject) => {
    url.on('value', (snapshot) => {
      if (snapshot.numChildren() > 0){
        dispatch({type: 'SET_JOIN', value: snapshot.val().isJoin})
        console.log('GETUSERJOIN', snapshot.val().isJoin)
      }else{
        dispatch({type: 'SET_JOIN', value: false})
        console.log('GETUSERJOIN', false)
      }
      resolve(snapshot.val())
    })
  })
}

export const getNumberJoin = () => (dispatch) => {
  const url = firebase.database().ref(`joinParty`)
  return new Promise((resolve, reject) => {
    url.on('value', snapshot => {
      const data = []
      if (snapshot.numChildren() > 0){
        Object.keys(snapshot.val()).map(key => {
          data.push({
            id: key,
            data: snapshot.val()[key]
          })
        })
      }
      let ret = 0
      data.forEach(item => {
        // console.log('ITEM ada', item.data)
        if (item.data.isJoin === true) ret ++
      })
      dispatch({type: 'SET_NUMBERJOIN', value: ret})
      console.log('UPDATE SET_NUMVERJOIN', ret)
      resolve(true)
    })
  })
}
