import React from 'react'
import {View, Text, ToastAndroid} from 'react-native'
import {Input, Button, Preloader} from '../component'

import {connect} from 'react-redux'
import {loginUser} from '../config/redux/action'

import styles from '../styles'
import firebase from '../config/firebase'

class Login extends React.Component {
  state = {
    email: '',
    password: '',
  }

  componentDidMount = () => {
    firebase.auth().onAuthStateChanged(user => {
      if (user){
        console.log('component didmount login user', user)
        this.props.navigation.navigate('Dashboard')
      }
    })  
  }

  clearState = () => {
    this.setState({
      email: '',
      password: '',
    })
  }

  handleLogin = async () => {
    const {email, password} = this.state
    const res = await this.props.loginUser({email, password})
      .catch(err => console.log(err))
    if (res){
      ToastAndroid.show('User logged in successfully', ToastAndroid.SHORT)
      this.clearState()
      this.props.getUserJoin({uid: user.uid})
      this.props.getNumberJoin()
      this.props.navigation.navigate('Dashboard')
    }else{
      ToastAndroid.show('Login failed', ToastAndroid.SHORT)
    } 
  }

  handleInput = (val, type) => {
    const state = this.state
    state[type] = val
    this.setState(state)
  }

  render() {
    const {email, password} = this.state
    const {isLoading} = this.props
    
    if (isLoading){
      return (
        <Preloader />
      )
    }  

    return (
      <View style = {styles.formContainer}>
        <Text style = {styles.logo}>Login</Text>
        <Input value = {email} placeholder = 'Email' onChangeText = {(val) => this.handleInput(val, 'email') }/>
        <Input value = {password} secureTextEntry = {true} placeholder = 'Password' onChangeText = {(val) => this.handleInput(val, 'password') }/>
        <Button title = 'Sign in' onPress = {this.handleLogin} />
        <Button title = {`Don't have an account? Sign Up`} secondary onPress = {() => this.props.navigation.navigate('Register')} />
      </View>
    )
  }
}

const reduxState = (state) => ({
  isLoading: state.isLoading,
})

const reduxDispatch = (dispatch) => ({
  loginUser: (data) => dispatch(loginUser(data)),
  setUser: (data) => dispatch(setUser(data))
})

export default connect(reduxState, reduxDispatch)(Login)