import React from 'react'
import {View, Text, ToastAndroid} from 'react-native'
import {Input, Button, Preloader} from '../component'

import {connect} from 'react-redux'
import {registerUser, loginUser} from '../config/redux/action'

import styles from '../styles'

class Register extends React.Component {
  state = {
    fullName: '',
    email: '',
    password: '',
  }

  clearState = () => {
    this.setState({
      fullName: '',
      email: '',
      password: '',
      isLoading: false,
    })
  }

  handleRegister = async () => {
    const {email, password, fullName} = this.state
    const res = await this.props.registerUser({email, password, fullName})
      .catch(err => console.log(err))
    if (res){
      ToastAndroid.show('User registered successfully', ToastAndroid.SHORT)
      this.handleLogin()      
    }else{
      ToastAndroid.show('Registration failed', ToastAndroid.SHORT)
    }
  }

  handleLogin = async () => {
    const {email, password} = this.state
    const res = await this.props.loginUser({email, password})
      .catch(err => console.log(err))
    if (res){
      ToastAndroid.show('User logged successfully', ToastAndroid.SHORT)
      this.clearState()
      this.props.navigation.navigate('Dashboard')
    }else{
      ToastAndroid.show('Login failed', ToastAndroid.SHORT)
    } 
  }

  handleInput = (val, type) => {
    const state = this.state
    state[type] = val
    this.setState(state)
  }

  render() {
    const {email, password, fullName} = this.state
    const {isLoading} = this.props

    if (isLoading) {
      return (
        <Preloader />
      )
    }

    return (
      <View style = {styles.formContainer}>
        <Text style = {styles.logo}>Register</Text>
        <Input value = {fullName} placeholder = 'Full Name' onChangeText = {(val) => this.handleInput(val, 'fullName')}/>
        <Input value = {email} placeholder = 'Email' onChangeText = {(val) => this.handleInput(val, 'email')}/>
        <Input value = {password} secureTextEntry = {true} placeholder = 'Password' onChangeText = {(val) => this.handleInput(val, 'password')}/>
        <Button title = 'Sign up' onPress = {this.handleRegister} />
        <Button title = 'Cancel' secondary onPress = {() => this.props.navigation.goBack()} />
      </View>
    )
  }
}

const reduxState = (state) => ({
  isLoading: state.isLoading,
})

const reduxDispatch = (dispatch) => ({
  registerUser: (data) => dispatch(registerUser(data)),
  loginUser: (data) => dispatch(loginUser(data))
})

export default connect(reduxState, reduxDispatch)(Register)