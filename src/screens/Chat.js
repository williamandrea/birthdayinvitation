import React from 'react'
import {View, Text, TextInput, TouchableOpacity, ScrollView} from 'react-native'
import styles from '../styles'
import {connect} from 'react-redux'
import {sendChat, getData, getChat} from '../config/redux/action'
import firebase from '../config/firebase'

class Chat extends React.Component {
  state = {
    text: '',
  }

  componentDidMount = () => {
    this.props.getChat()
  }

  handleInput = (val, type) => {
    const state = this.state
    state[type] = val
    this.setState(state)
  }

  handleSend = () => {
    const currentUser = firebase.auth().currentUser
    const chat = {
      uid: currentUser.uid,
      displayName: currentUser.displayName,
      timestamp: Date.now(),
      text: this.state.text,
    }
    this.props.sendChat(chat)
    this.setState({
      text: '',
    })
  }

  render() {
    const {text} = this.state
    const {chatList} = this.props
    return (
      <View style = {styles.chatContainer}>
        <View style = {styles.chatHeader}>
          <TouchableOpacity style = {styles.buttonBack} onPress = {() => this.props.navigation.goBack()}>
            <Text style = {styles.buttonBackText}>Back</Text>
          </TouchableOpacity>
          <Text style = {styles.chatTitle}>Discussion</Text>
        </View>
        <View style = {styles.chatContent}>
          <ScrollView>
            {
              chatList.map(item => {
                return (
                  <View style = {styles.chatItem}>
                    <Text style = {styles.chatDisplayName}>{item.data.displayName}</Text> 
                    <Text style = {styles.chatText}>{item.data.text}</Text> 
                  </View>
                )
              })
            }
          </ScrollView>
        </View>
        <View style = {styles.chatFooter}>
          <View style = {styles.chatForm}>
            <TextInput style = {styles.chatInput} value = {text} onChangeText = {(val) => this.handleInput(val, 'text')} placeholder = 'Enter a message... '/>
            <TouchableOpacity style = {styles.buttonSend} onPress = {this.handleSend}>
              <Text style = {styles.buttonSendText}>Send</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const reduxState = (state) => ({
  chatList: state.chatList,
})

const reduxDispatch = (dispatch) => ({
  sendChat: (data) => dispatch(sendChat(data)),
  getChat: () => dispatch(getChat())
})

export default connect(reduxState, reduxDispatch)(Chat)