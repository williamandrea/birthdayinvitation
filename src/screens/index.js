import Login from './Login'
import Register from './Register'
import Dashboard from './Dashboard'
import Chat from './Chat'

export {Login, Register, Dashboard, Chat}