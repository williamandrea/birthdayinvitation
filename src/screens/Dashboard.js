import React from 'react'
import {View, Text, StatusBar, Image, TouchableOpacity, ToastAndroid} from 'react-native'
import {Icon} from 'native-base'
import {ButtonDialog, ButtonLogout} from '../component'
import styles from '../styles'
import firebase from '../config/firebase'
import {connect} from 'react-redux'
import {setUserJoin, getUserJoin, getNumberJoin} from '../config/redux/action'

class Dashboard extends React.Component {
  componentDidMount = () => {
    const {user} = this.props
    console.log('DASHBOARD USER', user)
    this.props.getUserJoin({uid: user.uid})
    this.props.getNumberJoin()
  }

  handleLogout = () => {
    firebase.auth().signOut()
    this.props.navigation.navigate('Login')
  }

  handleJoin = () => {
    const {user} = this.props
    const {isJoin} = this.props
    if (isJoin){
      this.props.setUserJoin({uid: user.uid, isJoin: false})
    }else{
      this.props.setUserJoin({uid: user.uid, isJoin: true})
    }
  }

  render() {
    const {user} = this.props
    console.log('DASHBOARD CURRENTUSER', user)
    const {isJoin, numberJoin} = this.props
    console.log('DASHBOARD VALUE', isJoin, numberJoin)
    return (
      <View style = {styles.dashboardContainer}>
        <StatusBar backgroundColor = '#118EEB' />
        <View style = {styles.dashboardHeader}>
          <Text style = {styles.h2Title}>Hai {user.displayName},</Text>
          <ButtonLogout title = 'Logout' onPress = {this.handleLogout} />
        </View>
        <View style = {styles.imageContainer} />
        <View style = {styles.content}>
          <View style = {styles.mainContent}>
            <Image style = {styles.mainImage} source = {require('../assets/images/party.png')} />
            <Text style = {styles.h1}>WILLIAMANDREA'S</Text>
            <Text style = {styles.h1}>21TH BIRTHDAY PARTY</Text>
          </View>  
          <View style = {styles.restContent}>
            <View style = {styles.dialogContainer}>
              <Text style = {styles.h2}>{numberJoin} People registered</Text>
              <View style = {styles.row}>
                <ButtonDialog title = {isJoin? 'Cancel Join': 'Join'} onPress = {this.handleJoin}/>
              </View>
            </View>
          </View>
          <TouchableOpacity style = {styles.fab} onPress = {() => this.props.navigation.navigate('Chat')}>
            <Icon style = {styles.iconChat} type = 'MaterialCommunityIcons' name = 'chat-processing' />
          </TouchableOpacity>
        </View>
        
      </View>
    )
  }
}

const reduxState = (state) => ({
  numberJoin: state.numberJoin,
  isJoin: state.isJoin,
  user: state.user
})

const reduxDispatch = (dispatch) => ({
  setUserJoin: (data) => dispatch(setUserJoin(data)),
  getUserJoin: (data) => dispatch(getUserJoin(data)),
  getNumberJoin: (data) => dispatch(getNumberJoin()),
  
})

export default connect(reduxState, reduxDispatch)(Dashboard)

// export default Dashboard