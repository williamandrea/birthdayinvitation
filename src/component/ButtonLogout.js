import React from 'react'
import {TouchableOpacity, Text} from 'react-native'
import styles from '../styles'

const ButtonLogout = (props) => {
  return (
    <TouchableOpacity style = {[styles.buttonLogout, props.secondary? styles.buttonLogoutSecondary: null]} {...props}>
      <Text style = {[styles.buttonLogoutText, props.secondary? styles.buttonLogoutSecondaryText: null]}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export default ButtonLogout