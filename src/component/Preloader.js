import React from 'react'
import {ActivityIndicator, View} from 'react-native'

import styles from '../styles'

export default Preloader = () => {
  return (
    <View style = {styles.preloader}>
      <ActivityIndicator size = 'large' />
    </View>
  )
}