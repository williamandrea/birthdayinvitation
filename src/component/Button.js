import React from 'react'
import {TouchableOpacity, Text} from 'react-native'
import styles from '../styles'

const Button = (props) => {
  return (
    <TouchableOpacity style = {[styles.formButton, props.secondary? styles.formButtonSecondary: null]} {...props}>
      <Text style = {[styles.formButtonText, props.secondary? styles.formButtonSecondaryText: null]}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export default Button