import React from 'react'
import {TouchableOpacity, Text} from 'react-native'
import styles from '../styles'

const ButtonDialog = (props) => {
  return (
    <TouchableOpacity style = {[styles.buttonDialog, props.secondary? styles.buttonDialogSecondary: null]} {...props}>
      <Text style = {[styles.buttonDialogText, props.secondary? styles.buttonDialogSecondaryText: null]}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export default ButtonDialog