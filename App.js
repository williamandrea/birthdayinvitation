import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {Provider} from 'react-redux'
import {store} from './src/config/redux/store'

import {Login, Register, Dashboard, Chat} from './src/screens'

const Stack = createStackNavigator()

export default class App extends React.Component {
  render() {
    console.disableYellowBox = true
    return (
      <Provider store = {store}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName = 'Login' screenOptions = {{headerShown: false}}>
            <Stack.Screen name = 'Login' component = {Login} />
            <Stack.Screen name = 'Register' component = {Register} />
            <Stack.Screen name = 'Dashboard' component = {Dashboard} />
            <Stack.Screen name = 'Chat' component = {Chat} />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    )
  }
}